import '/imports/startup/client';
import { sAlert } from 'meteor/juliancwirko:s-alert';

Meteor.startup(function () {
  sAlert.config({ position: 'bottom-left', timeout: 3000, effect: 'slide' });
});

