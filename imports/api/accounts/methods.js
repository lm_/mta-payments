import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';
import { check } from 'meteor/check';
import { Session } from 'meteor/session';
import { Servers } from '../servers/servers.js';

Meteor.methods({
  "accounts/login": function(server, user, password){
    check(server, String);
    check(user, String);
    check(password, String);

    if (!(user&&password)) {
      throw new Meteor.Error(500, "Missing credentials");
    }
    let srv = Servers.findOne(server);
    let data = [user, password, srv.key]
    let accountData;
    try {
      accountData = HTTP.call("POST", "http://" + srv.host + ":" + srv.port + "/payments-api/call/login", { data: data, auth: srv.user + ":" + srv.password, timeout: 15000, headers: { "Content-Type":"application/json" } }) 
    } catch(error) {
      throw new Meteor.Error(500, error.message);
    }
    let response = JSON.parse(accountData.content);
    if (response[0].status == 1) {
      throw new Meteor.Error(500, response[0].message);
      return;
    }
    return response[0].data;
  },
  "accounts/changePassword": function(userId, password){
    check(userId, String);
    check(password, String);
    try {
      Accounts.setPassword(userId, password);
    } catch(error) {
      return false; 
    }
    return true;
  }
});

