import { Methods } from './p_methods.js';

Meteor.methods({
  'methods/create': function(name, type, currency, rate, id, key) {
    check(name, String);
    check(type, String);
    check(currency, String);
    check(rate, Number);
    check(id, String);
    check(key, String);
    try {
      Methods.insert({ name: name, type: type, currency: currency, rate: rate, id: id, key: key, enabled: true });
    } catch(error) {
      return false;
    }
    return true;
  },
  'methods/update': function(_id, name, type, currency, rate, id, key, enabled) {
    check(_id, String);
    check(name, String);
    check(type, String);
    check(currency, String);
    check(rate, Number);
    check(id, String);
    check(key, String);
    check(enabled, Boolean);
    try {
      Methods.update({_id: _id}, {
        $set: {
          name: name,
          type: type,
          currency: currency,
          rate: rate,
          id: id,
          key: key,
          enabled: enabled
        }
      });
    } catch(error) {
      return false;
    }
    return true;
  },
  'methods/remove': function(_id) {
    check(_id, String);
    try {
      Methods.remove(_id);
    } catch(error) {
      return false;
    }
    return true;
  }
});

