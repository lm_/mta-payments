import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Methods = new Mongo.Collection('Methods');

Methods.schema = new SimpleSchema({
  name: { type: String },
  type: { type: String },
  currency: { type: String },
  rate: { type: Number, decimal: true, optional: true },
  id: { type: String },
  key: { type: String, optional: true },
  enabled: { type: Boolean }
});

Methods.attachSchema(Methods.schema);

