import { Meteor } from 'meteor/meteor';
import { Methods } from '../p_methods.js';

Meteor.publish("Methods.private", function(){
  return Methods.find();
});

Meteor.publish("Methods.public", function(){
  return Methods.find({ enabled: true }, {
    fields: {
      name: 1,
      type: 1,
      currency: 1,
      rate: 1,
      id: 1
    }
  });
});

