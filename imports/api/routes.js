import { Meteor } from 'meteor/meteor';
import { Picker } from 'meteor/meteorhacks:picker';
import { CryptoJS } from 'meteor/jparker:crypto-core';
import { HTTP } from 'meteor/http';
import { Transactions } from '../api/transactions/transactions.js';
import { Methods } from '../api/methods/p_methods.js';

function getQueryParams(body) {
  let params = {};
  let query =  body.split('&');
  for (let i = 0; i < query.length; i++) {
    let kv = query[i].split('=');
    params[kv[0]] = kv[1];
  }  
  return params;
}

function checkAuthYad(params, receiver) {
  let method = Methods.findOne({ id: receiver });
  let p = params['notification_type'] + '&' + params['operation_id'] + '&' + params['amount'] + '&' + params['currency'] + '&' + params['datetime'] + '&' + params['sender'] + '&' + params['codepro'] + '&' + method.key + '&' + params['label'];
  let str = CryptoJS.SHA1(p).toString();
  let hex = CryptoJS.enc.Hex.parse(str);
  if (CryptoJS.enc.Hex.stringify(hex)===params['sha1_hash']) {
    return true;
  }
  return false;
}

function checkAuthWM(params) {
  let method = Methods.findOne({ id: params['LMI_PAYEE_PURSE'] });
  let p = params['LMI_PAYEE_PURSE'] + params['LMI_PAYMENT_AMOUNT'] + params['LMI_PAYMENT_NO'] + params['LMI_MODE'] + params['LMI_SYS_INVS_NO'] + params['LMI_SYS_TRANS_NO'] + params['LMI_SYS_TRANS_DATE'] + method.key + params['LMI_PAYER_PURSE'] + params['LMI_PAYER_WM'];
//  let str = CryptoJS.HmacSHA256(p, method.key).toString().toUpperCase();
  let str = CryptoJS.SHA256(p).toString().toUpperCase();
}

function checkWMOrigin(addr) {
  let p = addr.split('.');
  let str = p[0] + '.' + p[1] + '.' + p[2];
  if (str=='91.227.52'||str=='91.200.28'||str=='212.158.173'||str=='212.118.48') {
    return true;
  } else {
    return false;
  }
}

function fetchParams(amount, method, payer, receiver, server_account ) {
  if (amount&&method&&payer&&receiver&&server_account) {
    Meteor.call("transactions/create", parseFloat(amount), method, payer, receiver, server_account, function(err, result) {
      if (err) {
        console.log(err);
      }
      Meteor.call("transactions/perform", result, function(err2, result2){
        if (err2) {
          console.log(err2);
        }
      });
    });
  }
}

Picker.route('/payments/yad', function(params, req, res, next) {
  if(req.method=='POST') {
    let body = '';
    req.on('data', function(data) {
      body += data;
    });

    req.on('end', Meteor.bindEnvironment(function() {
      body = decodeURIComponent(body);
      let params = getQueryParams(body);
      let s = params['label'].split('`');
      if (checkAuthYad(params, s[0])) {
        fetchParams(params['amount'], "yad", params['sender'], s[0], s[1] + '`' + s[2]);
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end();
      } else {
        res.writeHead(401, {'Content-Type': 'text/plain'});
        res.end();
      }
    }));
  }
});

Picker.route('/payments/wm', function(params, req, res, next) {
  if (req.method=='POST') {
    let body = '';
    req.on('data', function (data) {
      body += data;
    });

    req.on('end', Meteor.bindEnvironment(function() {
      body = decodeURIComponent(body);
      let params = getQueryParams(body);
      if (params['LMI_PREREQUEST']==1) {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end("YES");
      } else {
//        checkAuthWM(params);
        if (checkWMOrigin(req.headers['x-forwarded-for']||req.connection.remoteAddress)) {
          fetchParams(params['LMI_PAYMENT_AMOUNT'], "wm", params['LMI_PAYER_PURSE'], params['LMI_PAYEE_PURSE'], params['LMI_PAYMENT_DESC']);
          res.writeHead(200, {'Content-Type': 'text/plain'});
          res.end();
        } else {
          res.writeHead(401, {'Content-Type': 'text/plain'});
          res.end();
        }
      }
    }));
  }
});

Picker.route('/payments/pp', function(params, req, res, next) {
  if (req.method=='POST') {
    let body = '';
    req.on('data', function (data) {
      body += data;
    });

    req.on('end', Meteor.bindEnvironment(function() {
      let body_decoded = decodeURIComponent(body);
      let params = getQueryParams(body_decoded);
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end();

      HTTP.call('POST', 'https://www.paypal.com/cgi-bin/webscr?cmd=_notify-validate&' + body, { headers: { 'User-Agent': params.receiver_id } }, function(err, response) {
        if (err) {
          throw new Meteor.Error(500, err.message);
        }
        if (response.content=='VERIFIED') {
          let t = Transactions.findOne({ pp_txn_id: params.txn_id });
          if ((!t) && params.payment_status=='Completed') {
            Meteor.call('transactions/create-pp', parseFloat(params.mc_gross) - parseFloat(params.mc_shipping) - parseFloat(params.tax), parseFloat(params.quantity || params.quantity1), params.payer_id, params.custom, params.txn_id, function(error, result){
              if (error) {
                console.log(err);
              }
              Meteor.call("transactions/perform", result, function(error2, result2){
                if (error2) {
                  console.log(error2);
                }
              });
            });
          }
        }
      });
    }));
  }
});

