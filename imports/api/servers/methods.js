import { Servers } from './servers.js';
import { HTTP } from 'meteor/http';

Meteor.methods({
  "servers/create": function(name, host, port, port_udp, user, password, key) {
    check(name, String);
    check(host, String);
    check(port, String);
    check(port_udp, String);
    check(user, String);
    check(password, String);
    check(key, String);

    let data;
    try {
      data = HTTP.call("POST", "http://" + host + ":" + port + "/payments-api/call/ping", { auth: user + ":" + password, timeout: 15000, headers: {
 "Content-Type":"application/json" } });
    } catch (error) {
      throw new Meteor.Error(500, error.message);
    }
    let response = JSON.parse(data.content);
    if (response[0].status==0) {
      try {
        Servers.insert({ name: name, host: host, port: port, port_udp: port_udp, user: user, password: password, key: key, enabled: true });
      } catch(error) {
        throw new Meteor.Error(500, error.message);
        return false;
      }
      return true;
    } else {
      throw new Meteor.Error(500, 'servers.alerts.unreachable');
      return;
    }
  },
  "servers/update": function(serverId, name, host, port, port_udp, user, password, key, enabled) {
    check(serverId, String);
    check(name, String);
    check(host, String);
    check(port, String);
    check(port_udp, String);
    check(user, String);
    check(password, String);
    check(key, String);
    check(enabled, Boolean);

    let data;
    try {
      data = HTTP.call("POST", "http://" + host + ":" + port + "/payments-api/call/ping", { auth: user + ":" + password, timeout: 15000, headers: {
 "Content-Type":"application/json" } });
    } catch (error) {
      throw new Meteor.Error(500, error.message);
    }
    let response = JSON.parse(data.content);
    if (response[0].status==0) {
      try {
        Servers.update({_id: serverId}, {
          $set: {
            name: name,
            host: host,
            port: port,
            port_udp: port_udp,
            user: user,
            password: password,
            key: key,
            enabled: enabled
          }
        });
      } catch(error) {
        return false;
      }
      return true;
    } else {
      throw new Meteor.Error(500, 'servers.alerts.unreachable');
      return;
    }
  },
  "servers/remove": function(serverId) {
    check(serverId, String);
    try {
      Servers.remove(serverId);
    } catch(error) {
      return false;
    }
    return true;
  }
});

