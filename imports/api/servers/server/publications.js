import { Meteor } from 'meteor/meteor';
import { Servers } from '../servers.js';

Meteor.publish("Servers.public", function() {
  return Servers.find({ enabled: true }, { fields: { name: 1, host: 1, port_udp: 1 } });
});

Meteor.publish("Servers.private", function() {
  return Servers.find();
});

