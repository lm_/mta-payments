import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Servers = new Mongo.Collection('Servers');

Servers.schema = new SimpleSchema({
  name: { type: String },
  host: { type: String },
  port: { type: String },
  port_udp: { type: String },
  user: { type: String },
  password: { type: String },
  key: { type: String },
  enabled: { type: Boolean }
});

Servers.attachSchema(Servers.schema);

