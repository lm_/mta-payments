import { Settings } from './settings.js';

Meteor.methods({
  "settings/update": function(autocomplete, name, email, logo, bg){
    check (autocomplete, Boolean);
    check (name, String);
    check (email, Match.Maybe(String));
    check (logo, String);
    check (bg, String);

    try {
      Settings.update({}, {
        $set: {
          autocomplete: autocomplete,
          name: name,
          email: email,
          logo_path: logo,
          bg_path: bg
        }
      });
    } catch(error) {
      return false;
    }
    return true;
  },
  "settings/update-enabled": function(autocomplete) {
    check (autocomplete, Boolean);

    try {
      Settings.update({}, {
        $set: {
          autocomplete: autocomplete,
        }
      });
    } catch (error) {
      return flase
    }
    return true;
  }
});

