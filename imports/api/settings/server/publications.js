import { Meteor } from 'meteor/meteor';
import { Settings } from '../settings.js';

Meteor.publish("Settings", function(){
  return Settings.find();
});

Meteor.publish("Settings.public", function(){
  return Settings.find({}, {
    fields: {
      name: 1,
      email: 1,
      bg_path: 1,
      logo_path: 1
    }
  });
});


