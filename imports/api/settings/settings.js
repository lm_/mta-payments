import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Settings = new Mongo.Collection('Settings');

Settings.schema = new SimpleSchema({
  autocomplete: { type: Boolean },
  name: { type: String },
  email: { type: String, optional: true },
  logo_path: { type: String },
  bg_path: { type: String }
});

Settings.attachSchema(Settings.schema);

