import { Transactions } from './transactions.js';
import { Methods } from '../methods/p_methods.js';
import { Servers } from '../servers/servers.js';

Meteor.methods({
  "transactions/create": function(sum, payment_method, payer, receiver, server_account) {
    check(sum, Number);
    check(payment_method, String);
    check(payer, String);
    check(receiver, String);
    check(server_account, String);

    let method = Methods.findOne({ id: receiver });
    let amount = sum * method.rate;
/*    if (!(amount^0===amount)) {
      let decimal = amount - Math.floor(amount);
      if (decimal > 0.9) {
        amount = Math.ceil(amount);
      }
    } */
    amount = amount.toFixed();
    let srv_acc = server_account.split('`');
    let transaction = Transactions.insert({ 
      sum: sum, 
      amount: amount, 
      payment_method: payment_method, 
      payer: payer, 
      receiver: receiver,
      performed: false,
      server: srv_acc[0], 
      account: srv_acc[1] 
    });
    return transaction;
  },
  "transactions/create-pp": function(sum, amount, payer, receiver_server_account, txn_id) {
    check(sum, Number);
    check(amount, Number);
    check(payer, String);
    check(receiver_server_account, String);
    check(txn_id, String);
    let s = receiver_server_account.split('`');
    let transaction = Transactions.insert({
      sum: sum,
      amount: amount.toFixed(),
      payment_method: 'pp',
      payer: payer,
      receiver: s[0],
      performed: false,
      server: s[1],
      account: s[2],
      pp_txn_id: txn_id
    });
    return transaction;
    
  },
  "transactions/perform": function(id) {
    check(id, String);

    let transaction = Transactions.findOne(id);
    let server = Servers.findOne(transaction.server);
    let data = [transaction.account, server.key, transaction.amount];
    let responseData;
    try {
      responseData = HTTP.call("POST", "http://" + server.host + ":" + server.port + "/payments-api/call/transaction", { data: data, auth: server.user + ":" + server.password, headers: { "Content-Type":"application/json" } })
    } catch (error) {
      Transactions.update( { _id: id }, {
        $set: {
          err_message: error.message
        }
      });
    }
    let response = JSON.parse(responseData.content);
    if (response[0].status == 1) {
      Transactions.update( { _id: id }, {
        $set: {
          err_message: response[0].message
        }
      });
    }
    Transactions.update( { _id: id }, {
      $set: {
        err_message: "",
        performed: true,
      }
    });
    return true;
  },
  "transactions/autocomplete": function() {
    console.log("autocomplete");
  }
});

