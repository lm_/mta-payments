import { Meteor } from 'meteor/meteor';
import { Transactions } from '../transactions.js';

Meteor.publish("Transactions", function(){
  return Transactions.find();
});

Meteor.publish("Transactions.filtered", function(mode){
  check(mode, Number);
  if (mode==0) {
    return Transactions.find({}, { limit: 15 });
  } else if (mode==1) {
    return Transactions.find({ performed: false });
  } else {
    return Transactions.find();
  }
});

