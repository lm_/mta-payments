import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Transactions = new Mongo.Collection('Transactions');

Transactions.schema = new SimpleSchema({
  sum: { type: Number, decimal: true },
  amount: { type: Number },
  payment_method: { type: String },
  payer: { type: String },
  receiver: { type: String },
  date_time_received: { type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();
      }
    }
  },
  date_time_performed: { 
    type: Date,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true
  },
  performed: { type: Boolean },
  server: { type: String },
  account: { type: String },
  err_message: { type: String, optional: true },
  pp_txn_id: { type: String, optional: true }
});

Transactions.attachSchema(Transactions.schema);

