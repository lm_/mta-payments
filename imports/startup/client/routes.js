import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { Session } from 'meteor/session';

import '../../ui/layouts/body.js';
import '../../ui/pages/main-page.js';
import '../../ui/pages/login-page.js';
import '../../ui/pages/account-page.js';
import '../../ui/pages/recharge-page.js';
import '../../ui/pages/manage-page.js';
import '../../ui/pages/sign-in-page.js';
import '../../ui/pages/denied-page.js';
import '../../ui/pages/change-password-page.js';
import '../../ui/pages/not-found-page.js';
import '../../ui/pages/servers-page.js';
import '../../ui/pages/transactions-page.js';
import '../../ui/pages/methods-page.js';
import '../../ui/components/lang-panel.js';
import '../../ui/components/server-list.js';
import '../../ui/components/server-list-item.js';
import '../../ui/components/server-list-item-details.js';
import '../../ui/components/server-list-item-new.js';
import '../../ui/components/methods-list.js';
import '../../ui/components/methods-list-item.js';
import '../../ui/components/methods-list-item-new.js';
import '../../ui/components/methods-list-item-details.js';
import '../../ui/components/transactions-list.js';
import '../../ui/components/transactions-list-item.js';
import '../../ui/components/transactions-list-item-details.js';
import '../../ui/components/wm-form.js';
import '../../ui/components/yad-form.js';
import '../../ui/components/pp-form.js';

FlowRouter.wait();
Tracker.autorun(function(){
  if (Roles.subscription.ready()&&!FlowRouter._initialized){
    FlowRouter.initialize();
  }
});

exposed = FlowRouter.group({
  name: 'App.public'
});

exposed.route('/', {
  name: 'App.public.home',
  action() {
    BlazeLayout.render('App_body', { main: 'Main_page' });
  },
});

exposed.route('/sign-in', {
  name: 'App.public.sign-in',
  action() {
    BlazeLayout.render('App_body', { main: 'Sign_in_page' });
  },
});

exposed.route('/recharge-wm', {
  name: 'App.public.recharge-wm',
  action() {
     BlazeLayout.render('App_body', { main: 'Recharge_page', form: 'WM_form' });
  }
});

exposed.route('/recharge-yad', {
  name: 'App.public.recharge-yad',
  action() {
     BlazeLayout.render('App_body', { main: 'Recharge_page', form: 'Yad_form' });
  }
});

exposed.route('/recharge', {
  name: 'App.public.recharge',
  action: function() {
    let id = Session.get("selected_method");
    if (id) {
      BlazeLayout.render('App_body', { main: 'Recharge_page' });
    } else {
      BlazeLayout.render('App_body', { main: 'Not_found_page' });
    }
  }
});

protected = FlowRouter.group({
  name: 'App.private',
  triggersEnter: [
    function() {
      if (!(Meteor.loggingIn() || Meteor.userId())) {
        let route = FlowRouter.current();
        if (route.route.name !== 'App.public.sign-in') {
          Session.set('redirectAfterLogin', route.path);
        }
        return FlowRouter.go('App.public.sign-in');
      }
    }, 
  ],
});

protected.route('/denied', {
  name: 'App.private.denied',
  action() {
    BlazeLayout.render('App_body', { main: 'Denied_page' });
  },
});

restricted = protected.group({
  prefix: '/manage',
  triggersEnter: [
    function() {
      if (Meteor.userId()) {
        if (!Roles.userIsInRole(Meteor.user(), 'manage', 'payment')) {
          console.log(Meteor.user());
          return FlowRouter.go('App.private.denied');
        }
      }
    }
  ],
});

restricted.route('/', {
  name: 'App.private.manage',
  action() {
    BlazeLayout.render('App_body', { main: 'Manage_page' });
  }
});

restricted.route('/password', {
  name: 'App.private.change-password',
  action() {
    BlazeLayout.render('App_body', { main: 'Change_password_page' });
  }
});

restricted.route('/servers', {
  name: 'App.private.servers',
  action() {
    BlazeLayout.render('App_body', { main: 'Servers_page' });
  }
});

restricted.route('/transactions', {
  name: 'App.private.transactions',
  action() {
    BlazeLayout.render('App_body', { main: 'Transactions_page' });
  }
});

restricted.route('/methods', {
  name: 'App.private.methods',
  action() {
    BlazeLayout.render('App_body', { main: 'Methods_page' });
  }
});

FlowRouter.notFound = {
  action() {
    BlazeLayout.render('App_body', { main: 'Not_found_page' });
  }
}

