import { Meteor } from 'meteor/meteor';
import { Settings } from '../../api/settings/settings.js';
import { Servers } from '../../api/servers/servers.js';

Meteor.startup(function() {
  if (Settings.find().count() === 0) {
    let setting = {
      autocomplete: true,
      name: 'MTA Payments',
      email: '',
      logo_path: '/assets/logo.png',
      bg_path: '/assets/bg.jpg'
    }
    Settings.insert(setting);
  }

  if (Servers.find().count() === 0) {
    let server = {
      name: "default",
      host: "46.188.35.85",
      port: "22005",
      user: "apiuser",
      password: "apiuser123",
      port_udp: "22003",
      key: "armaz.alkarik",
      enabled: true
    }
    Servers.insert(server);
  }

  if (Meteor.users.find().count() === 0) {
    userId = Accounts.createUser({
      username: "admin",
      password: "FhvzP123"
    });
    Roles.addUsersToRoles(userId, ["manage"], "payment");
  }
});

