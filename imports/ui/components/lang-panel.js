import { Template } from 'meteor/templating';
import { TAPi18n } from 'meteor/tap:i18n';
import './lang-panel.html';

Template.Lang_panel.helpers({
  langs: function() { 
    let l = TAPi18n.getLanguages();
    let langs = [];
    for (i = 0; i < Object.keys(l).length; i++) {
      langs.push({ key: Object.keys(l)[i], name: l[Object.keys(l)[i]].name });
    }
    return langs;
  }
});


Template.Lang_panel.events({
  'change [id="lang-panel"]': function(event, template) {
    event.stopPropagation();
    event.preventDefault();
    TAPi18n.setLanguage(event.target.value);
  }
});

