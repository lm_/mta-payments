import { Template } from 'meteor/templating';
import { TAPi18n } from 'meteor/tap:i18n';
import './methods-list-item-details.html';

Template.Methods_list_item_details.helpers({
  selected: function(m) {
    return this.type == m;
  }
});

Template.Methods_list_item_details.events({
  'click [data-event-action=method-save]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let name = template.$('input[id="method-name"]').val();
    let type = template.$('select[id="method-type"]').val();
    let currency = template.$('input[id="method-currency"]').val();
    let rate = parseFloat(template.$('input[id="method-rate"]').val());
    let id = template.$('input[id="method-id"]').val();
    let key = template.$('input[id="method-key"]').val();
    let enabled = template.$('input[id="method-enabled"]').is(':checked');

    let condition = false;
    if (type=='pp') {
      if (name&&currency&&id) {
        condition = true;
      }
    } else {
      if (name&&currency&&rate&&id) {
        condition = true;
      }
    }
    if (condition) {
      return Meteor.call("methods/update", this._id, name, type, currency, rate, id, key, enabled, function(error, result){
        if(error){
          sAlert.error(error);
          return;
        }
        sAlert.success(TAPi18n.__('methods.alerts.saved'));
      });
    } else {
      sAlert.error(TAPi18n.__('methods.alerts.parameters'));
    }
  },
});

