import { Template } from 'meteor/templating';
import { TAPi18n } from 'meteor/tap:i18n';
import './methods-list-item-new.html';

Template.Methods_list_item_new.events({
  'click [data-event-action=new-method-add]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let name = template.$('input[id="new-method-name"]').val();
    let type = template.$('select[id="new-method-type"]').val();
    let currency = template.$('input[id="new-method-currency"]').val();
    let rate = parseFloat(template.$('input[id="new-method-rate"]').val());
    let id = template.$('input[id="new-method-id"]').val();
    let key = template.$('input[id="new-method-key"]').val();

    let condition = false;
    if (type=='pp') {
      if (name&&currency&&id) {
        condition = true;
      }
    } else {
      if (name&&currency&&rate&&id) {
        condition = true;
      }
    }
    if (condition) {
      return Meteor.call("methods/create", name, type, currency, rate, id, key, function(error, result){
        if(error){
          sAlert.error(error);
          return;
        }
        sAlert.success(TAPi18n.__('methods.alerts.success'));
        let methodsList = Session.get("methods_list");
        if (methodsList.indexOf("new") != -1) {
          methodsList.splice(methodsList.indexOf("new"), 1);
          Session.set("methods_list", methodsList);
        } 
      });
    } else {
      sAlert.error(TAPi18n.__('methods.alerts.parameters'));
    }
  },
});

