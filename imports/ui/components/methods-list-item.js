import { Template } from 'meteor/templating';
import { Methods } from '../../api/methods/p_methods.js';
import { TAPi18n } from 'meteor/tap:i18n';
import './methods-list-item.html';

Template.Methods_list_item.onCreated(function() {
  this.subscribe("Methods.private");
});

Template.Methods_list_item.helpers({
  isOpen: function() {
    let methodsList = Session.get("methods_list");
    if (methodsList.indexOf(this._id) != -1) {
      return true;
    }
  }
});

Template.Methods_list_item.events({
  'click [data-event-action=method-edit]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let methodsList = Session.get("methods_list");
    if (methodsList.indexOf(this._id) == -1) {
      methodsList.push(this._id);
      Session.set("methods_list", methodsList);
    }
  },
  'click [data-event-action=method-remove]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    return Meteor.call("methods/remove", this._id, function(err, result) {
     if(err){
        sAlert.error(err);
        return;
      }
      sAlert.success(TAPi18n.__('methods.alerts.removed'));
    });
  },
  'click [data-event-action=method-close]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let methodsList = Session.get("methods_list");
    if (methodsList.indexOf(this._id) != -1) {
      methodsList.splice(methodsList.indexOf(this._id), 1);
      Session.set("methods_list", methodsList);
    }
  },
});

