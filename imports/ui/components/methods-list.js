import { Template } from 'meteor/templating';
import { Methods } from '../../api/methods/p_methods.js';

import './methods-list.html';

Template.Methods_list.onCreated(function() {
  this.subscribe("Methods.private");
  Session.set("methods_list", []);
});

Template.Methods_list.helpers({
  methods: function() {
    return Methods.find();
  },
  isOpen: function() {
    let methodsList = Session.get("methods_list");
    if (methodsList.indexOf("new") != -1) {
      return true;
    }
  },
});

Template.Methods_list.events({
  'click [data-event-action=method-add]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let methodsList = Session.get("methods_list");
    if (methodsList.indexOf("new") == -1) {
      methodsList.push("new");
      Session.set("methods_list", methodsList);
    }
  },
  'click [data-event-action=method-close]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let methodsList = Session.get("methods_list");
    if (methodsList.indexOf("new") != -1) {
      methodsList.splice(methodsList.indexOf("new"), 1);
      Session.set("methods_list", methodsList);
    }
  }
});

