import { Template } from 'meteor/templating';
import './pp-form.html';

Template.PP_form.helpers({
  server: function() {
    let data = Session.get("account_data");
    return data.server;
  },
  name: function() {
    let data = Session.get("account_data");
    return data.name;
  }
});

Template.PP_form.events({
});

