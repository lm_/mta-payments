import { Template } from 'meteor/templating';
import { TAPi18n } from 'meteor/tap:i18n';
import './server-list-item-details.html';

Template.Server_list_item_details.events({
  'click [data-event-action=server-save]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let name = template.$('input[id="server-name"]').val();
    let host = template.$('input[id="server-host"]').val();
    let port = template.$('input[id="server-port"]').val();
    let port_udp = template.$('input[id="server-port-udp"]').val();
    let user = template.$('input[id="server-user"]').val();
    let password = template.$('input[id="server-password"]').val();
    let key = template.$('input[id="server-key"]').val();
    let enabled = template.$('input[id="server-enabled"]').is(':checked');
    
    if (name&&host&&port&&port_udp&&user&&password&&key) {
      sAlert.info(TAPi18n.__('servers.alerts.save'), { timeout: 'none' });
      template.$('button[id="server-save-button"]').css('pointer-events', 'none');
      return Meteor.call("servers/update", this._id, name, host, port, port_udp, user, password, key, enabled, function(error, result){
        sAlert.closeAll();
        if(error){
          sAlert.error(error);
          template.$('button[id="server-save-button"]').css('pointer-events', 'auto');
          return;
        }
        sAlert.success(TAPi18n.__('servers.alerts.saved'));
        template.$('button[id="server-save-button"]').css('pointer-events', 'auto');
      });
    } else {
      sAlert.error(TAPi18n.__('servers.alerts.parameters'));
    }
  },
});

