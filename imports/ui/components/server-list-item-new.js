import { Template } from 'meteor/templating';
import { TAPi18n } from 'meteor/tap:i18n';
import './server-list-item-new.html';

Template.Server_list_item_new.events({
  'click [data-event-action=new-server-add]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let name = template.$('input[id="new-server-name"]').val();
    let host = template.$('input[id="new-server-host"]').val();
    let port = template.$('input[id="new-server-port"]').val();
    let port_udp = template.$('input[id="new-server-port-udp"]').val();
    let user = template.$('input[id="new-server-user"]').val();
    let password = template.$('input[id="new-server-password"]').val();
    let key = template.$('input[id="new-server-key"]').val();
    
    if (name&&host&&port&&port_udp&&user&&password&&key) { 
      sAlert.info(TAPi18n.__('servers.alerts.add'), { timeout: 'none' });
      template.$('button[id="server-add-button"]').css('pointer-events', 'none');
      return Meteor.call("servers/create", name, host, port, port_udp, user, password, key, function(error, result){
        sAlert.closeAll();
        if(error){
          sAlert.error(TAPi18n.__(error.reason));
          template.$('button[id="server-add-button"]').css('pointer-events', 'auto');
          return;
        }
        sAlert.success(TAPi18n.__('servers.alerts.success'));
        let serverList = Session.get("server_list");
        if (serverList.indexOf("new") != -1) {
          serverList.splice(serverList.indexOf("new"), 1);
          Session.set("server_list", serverList);
        }
        template.$('button[id="server-add-button"]').css('pointer-events', 'auto');
      });
    } else {
      sAlert.error(TAPi18n.__('servers.alerts.parameters'));
    }
  },
});

