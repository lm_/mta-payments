import { Template } from 'meteor/templating';
import { Servers } from '../../api/servers/servers.js';
import { TAPi18n } from 'meteor/tap:i18n';
import './server-list-item.html';

Template.Server_list_item.onCreated(function() {
  this.subscribe("Servers.private");
});

Template.Server_list_item.helpers({
  isOpen: function() {
    let serverList = Session.get("server_list");
    if (serverList.indexOf(this._id) != -1) {
      return true;
    }
  }
});

Template.Server_list_item.events({
  'click [data-event-action=server-edit]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let serverList = Session.get("server_list");
    if (serverList.indexOf(this._id) == -1) {
      serverList.push(this._id);
      Session.set("server_list", serverList);
    }
  },
  'click [data-event-action=server-remove]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    return Meteor.call("servers/remove", this._id, function(err, result) {
     if(err){
        sAlert.error(err);
        return;
      }
      sAlert.success(TAPi18n.__('servers.alerts.removed'));
    });
  },
  'click [data-event-action=server-close]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let serverList = Session.get("server_list");
    if (serverList.indexOf(this._id) != -1) {
      serverList.splice(serverList.indexOf(this._id), 1);
      Session.set("server_list", serverList);
    }
  },
});

