import { Template } from 'meteor/templating';
import { Servers } from '../../api/servers/servers.js';

import './server-list.html';

Template.Server_list.onCreated(function() {
  this.subscribe("Servers.private");
  Session.set("server_list", []);
});

Template.Server_list.helpers({
  servers: function() {
    return Servers.find();
  },
  isOpen: function() {
    let serverList = Session.get("server_list");
    if (serverList.indexOf("new") != -1) {
      return true;
    }
  },
});

Template.Server_list.events({
  'click [data-event-action=server-add]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let serverList = Session.get("server_list");
    if (serverList.indexOf("new") == -1) {
      serverList.push("new");
      Session.set("server_list", serverList);
    }
  },
  'click [data-event-action=server-close]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let serverList = Session.get("server_list");
    if (serverList.indexOf("new") != -1) {
      serverList.splice(serverList.indexOf("new"), 1);
      Session.set("server_list", serverList);
    }
  }
});

