import { Template } from 'meteor/templating';
import { Servers } from '../../api/servers/servers.js';

import './transactions-list-item-details.html';

Template.Transactions_list_item_details.onCreated(function() {
  this.subscribe("Servers.private");
});

Template.Transactions_list_item_details.helpers({
  method: function() {
    let methods = {
      wm: 'WebMoney',
      qiwi: 'QIWI',
      pp: 'PayPal',
      yad: 'Yandex.Money'
    }
    return methods[this.payment_method];
  },
  notPerformed: function() {
    if (this.performed) {
      return false;
    }
    return true;
  },
  srv: function() {
    let server = Servers.findOne(this.server);
    return server.name + ' (' + server.host + ':' + server.port_udp + ')';
  }
});

Template.Transactions_list_item_details.events({
  'click [data-event-action=transaction-complete]': function(event, template) {
    event.stopPropagation();
    event.preventDefault();

    Meteor.call("transactions/perform", this._id, function(err, result){
      if (err) {
        sAlert.error(err);
        return;
      }
      sAlert.warning("Transaction completion attempt was performed, check status.")
    });
  }
});

