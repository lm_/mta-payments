import { Template } from 'meteor/templating';
import { Transactions } from '../../api/transactions/transactions.js';

import './transactions-list-item.html';

Template.Transactions_list_item.helpers({
  notPerformed: function() {
    if(this.performed) {
      return false;
    }
    return true;
  },
  isOpen: function() {
    let transactionsList = Session.get("transactions_list");
    if (transactionsList.indexOf(this._id) != -1) {
      return true;
    }
  }
});

Template.Transactions_list_item.events({
  'click [data-event-action=transaction-details]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let transactionsList = Session.get("transactions_list");
    if (transactionsList.indexOf(this._id) == -1) {
      transactionsList.push(this._id);
      Session.set("transactions_list", transactionsList);
    }
  },
  'click [data-event-action=transaction-close]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let transactionsList = Session.get("transactions_list");
    if (transactionsList.indexOf(this._id) != -1) {
      transactionsList.splice(transactionsList.indexOf(this._id), 1);
      Session.set("transactions_list", transactionsList);
    }
  },
});

