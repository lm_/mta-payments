import { Template } from 'meteor/templating';
import { Transactions } from '../../api/transactions/transactions.js';

import './transactions-list.html';

Template.Transactions_list.onCreated(function() {
//  this.subscribe("Transactions");
  Session.set("transactions_list", []);
  Session.set("transactions_list_mode", 0);
});

Template.Transactions_list.onRendered(function() {
  let self = this;
  self.autorun(function(){
    self.subscribe("Transactions.filtered", Session.get("transactions_list_mode"));
  });
});

Template.Transactions_list.helpers({
  count: function() {
    return Transactions.find().count() > 0;
  },
  transactions: function() {
    return Transactions.find({}, { sort: { date_time_received: -1 }});
  },
  mode: function(m) {
    return Session.get("transactions_list_mode") == m;
  }
});

Template.Transactions_list.events({
  'click [data-event-action=transaction-filter-15]': function(event, template) {
    event.stopPropagation();
    event.preventDefault();

    let mode = Session.get("transactions_list_mode");
    if (mode!=0) {
      Session.set("transactions_list_mode", 0);
    }
  },
  'click [data-event-action=transaction-filter-incomplete]': function(event, template) {
    event.stopPropagation();
    event.preventDefault();

    let mode = Session.get("transactions_list_mode");
    if (mode!=1) {
      Session.set("transactions_list_mode", 1);
    }
  },
  'click [data-event-action=transaction-filter-all]': function(event, template) {
    event.stopPropagation();
    event.preventDefault();

    let mode = Session.get("transactions_list_mode");
    if (mode!=2) {
      Session.set("transactions_list_mode", 2);
    }
  },
});

