import { Template } from 'meteor/templating';

import './wm-form.html';

Template.WM_form.helpers({
  initial_cost: function() {
    let initial = 1 / this.rate;
    return initial.toFixed(2);
  },
  server: function() {
    let data = Session.get("account_data");
    return data.server;
  },
  name: function() {
    let data = Session.get("account_data");
    return data.name;
  }
});

Template.WM_form.events({
  'change [id="wm-amount"]': function(event, template) {
    event.stopPropagation();
    event.preventDefault();
    let amount = template.$('input[id="wm-amount"]').val();
    let cost = amount / this.rate;
    template.$('input[id="wm-cost"]').val(cost.toFixed(2));
    template.$('input[name="LMI_PAYMENT_AMOUNT"]').val(cost.toFixed(2));
  },
});

