import { Template } from 'meteor/templating';

import './yad-form.html';

Template.Yad_form.helpers({
  initial_cost: function() {
    let initial = (1 / this.rate) + (1 / this.rate * 0.005);
    return initial.toFixed(2);
  },
  server: function() {
    let data = Session.get("account_data");
    return data.server;
  },
  name: function() {
    let data = Session.get("account_data");
    return data.name;
  },
  successUrl: function() {
    return window.location.host;
  }
});

Template.Yad_form.events({
  'change [id="yad-amount"]': function(event, template) {
    event.stopPropagation();
    event.preventDefault();
    let amount = template.$('input[id="yad-amount"]').val();
    let cost = amount / this.rate + amount / this.rate * 0.005;
    template.$('input[id="yad-cost"]').val(cost.toFixed(2));
    template.$('input[name="sum"]').val(cost.toFixed(2));
  },
});

