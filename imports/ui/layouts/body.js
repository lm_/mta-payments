import { Template } from 'meteor/templating';
import { Settings } from '../../api/settings/settings.js';
import './body.html';

Template.App_body.onCreated(function(){
  this.subscribe('Settings.public');
});

Template.App_body.helpers({
  logo_path: function() {
    let settings = Settings.findOne();
    document.title = settings.name;
    return settings.logo_path; 
  }
});

