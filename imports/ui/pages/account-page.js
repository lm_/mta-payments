import { Template } from 'meteor/templating';
import { Methods } from '../../api/methods/p_methods.js';

import './account-page.html';

Template.Account_page.onCreated(function() {
  this.subscribe("Methods.public");
});

Template.Account_page.helpers({
  accountData: function() {
    return Session.get("account_data");
  },
  methods: function() {
    return Methods.find();
  }
});

Template.Account_page.events({
  'click [data-event-action=logout]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
    Session.set("account_data", false);
  },
  'click [data-event-action=recharge]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    Session.set("selected_method", event.target.id);
    FlowRouter.go('App.public.recharge');
  }
});

