import { Template } from 'meteor/templating';
import './change-password-page.html';

Template.Change_password_page.events({
  'submit form': function(event, template){
    event.preventDefault();
    event.stopPropagation();

    let password = template.$('input[id="change-password-field"]').val();
    let confirm = template.$('input[id="change-password-confirm"]').val();

    if (!password||!confirm) {
      sAlert.error("Both fields should be filled");
      return;
    }
    if (password!=confirm) {
      sAlert.error("Password and confirmation don't match");
      return;
    }
    Meteor.call("accounts/changePassword", Meteor.userId(), password, function(error, result) {
      if (error) {
        sAlert.error(error);
      }
      if (result) {
        sAlert.success("New password has been saved");
      }
    });
  },
  'click [data-event-action=back]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Meteor.logout();
    FlowRouter.go("App.public.home");
  }
});

