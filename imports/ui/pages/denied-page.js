import { Template } from 'meteor/templating';

import './denied-page.html';

Template.Denied_page.events({
  'click [data-event-action=back]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Meteor.logout();
    FlowRouter.go("App.public.home");
  }
});

