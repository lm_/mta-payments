import { Template } from 'meteor/templating';
import { sAlert } from 'meteor/juliancwirko:s-alert';
import { Servers } from '../../api/servers/servers.js';
import { TAPi18n } from 'meteor/tap:i18n';
import './login-page.html';

Template.Login_page.onCreated(function() {
  this.subscribe("Servers.public");
//  TAPi18n.setLanguage("ru");
});


Template.Login_page.helpers({
  servers: function() {
    return Servers.find();
  }
});

Template.Login_page.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
    
    let user = template.$('input[id="login-username"]').val();
    let password = template.$('input[id="login-password"]').val();
    let server = template.$('select[id="login-server"]').val();
    sAlert.info(TAPi18n.__('account.alerts.login'), { timeout: 'none' });
    template.$('button[id="login-button"]').css('pointer-events', 'none');
    return Meteor.call("accounts/login", server, user, password, function(error, result){
      sAlert.closeAll();
      if(error){
        sAlert.error(TAPi18n.__(error.reason));
        template.$('button[id="login-button"]').css('pointer-events', 'auto');
        return;
      }
      result.server = server;
      Session.set("account_data", result);
      template.$('button[id="login-button"]').css('pointer-events', 'auto');
    });
  }
});

