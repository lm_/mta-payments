import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';

import './main-page.html';

Template.Main_page.helpers({
  userLoggedIn: function(){
    return Session.get("account_data");
  }
});

Template.Main_page.events({
  'click [data-event-action=manage]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.manage");
  }
});

