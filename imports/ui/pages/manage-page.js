import { Template } from 'meteor/templating';
import { TAPi18n } from 'meteor/tap:i18n';
import { Settings } from '../../api/settings/settings.js';

import './manage-page.html';

Template.Manage_page.onCreated(function(){
  this.subscribe("Settings");
});

Template.Manage_page.helpers({
  setting: function(){
    return Settings.findOne();
  }
});

Template.Manage_page.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
    
    let autocomplete = template.$('input[id="settings-autocomplete"]').is(':checked');
    let name = template.$('input[id="settings-name"]').val();
    let email = template.$('input[id="settings-email"]').val();
    let bg = template.$('input[id="settings-bg"]').val();
    let logo = template.$('input[id="settings-logo"]').val();

    Meteor.call("settings/update", autocomplete, name, email, logo, bg, function(error, result){
      if(error){
         sAlert.error(error);
      }
      if(result){
        sAlert.success(TAPi18n.__('admin.settings-saved'));
      }
    })
  },
  'click [data-event-action=manage-checkbox]': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let autocomplete = template.$('input[id="settings-autocomplete"]').is(':checked');

    Meteor.call("settings/update-enabled", autocomplete, function(error, result) {
      if (error) {
        sAlert.error(error);
      }
    })
  },
  'click [data-event-action=servers]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.servers");
  },
  'click [data-event-action=transactions]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.transactions");
  },
  'click [data-event-action=change-password]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.change-password");
  },
  'click [data-event-action=methods]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.methods");
  },
  'click [data-event-action=back]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Meteor.logout();
    FlowRouter.go("App.public.home");
  }
});

