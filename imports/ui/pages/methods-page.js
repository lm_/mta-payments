import { Template } from 'meteor/templating';

import './methods-page.html';

Template.Methods_page.events({
  'click [data-event-action=manage]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.manage");
  },
  'click [data-event-action=transactions]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.transactions");
  },
  'click [data-event-action=change-password]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.change-password");
  },
  'click [data-event-action=servers]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.servers");
  },
  'click [data-event-action=back]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Meteor.logout();
    FlowRouter.go("App.public.home");
  }
});

