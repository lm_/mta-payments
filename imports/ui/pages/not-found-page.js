import { Template } from 'meteor/templating';

import './not-found-page.html';

Template.Not_found_page.events({
  'click [data-event-action=back]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.public.home");
  }
});

