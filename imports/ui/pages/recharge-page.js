import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';
import { Methods } from '../../api/methods/p_methods.js';

import './recharge-page.html';

Template.Recharge_page.onCreated(function(){
  this.subscribe("Methods.public");
});

Template.Recharge_page.helpers({
  userLoggedIn: function(){
    return Session.get("account_data");
  },
  method: function() {
    return Methods.findOne(Session.get("selected_method"));
  },
  type: function(p) {
    return this.type == p;
  }
});

Template.Recharge_page.events({
  'click [data-event-action=back]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Meteor.logout();
    FlowRouter.go("App.public.home");
  }
});

