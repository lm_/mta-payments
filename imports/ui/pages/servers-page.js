import { Template } from 'meteor/templating';

import './servers-page.html';

Template.Servers_page.events({
  'click [data-event-action=manage]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.manage");
  },
  'click [data-event-action=transactions]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.transactions");
  },
  'click [data-event-action=change-password]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.change-password");
  },
  'click [data-event-action=methods]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.private.methods");
  },
  'click [data-event-action=back]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    Meteor.logout();
    FlowRouter.go("App.public.home");
  }
});

