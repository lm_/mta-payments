import { Template } from 'meteor/templating';
import { sAlert } from 'meteor/juliancwirko:s-alert';
import { TAPi18n } from 'meteor/tap:i18n';
import './sign-in-page.html';

Template.Sign_in_page.events({
  'submit form': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    let user = template.$('input[id="sign-in-username"]').val();
    let password = template.$('input[id="sign-in-password"]').val();
    if (user&&password) {
      template.$('button[id="sign-in-button"]').css('pointer-events', 'none');
      Meteor.loginWithPassword(user, password, function(error, result) {
        if (error) {
          sAlert.error(error);
          template.$('button[id="sign-in-button"]').css('pointer-events', 'auto');
          return;
        }
        template.$('button[id="sign-in-button"]').css('pointer-events', 'auto');
        FlowRouter.go(Session.get("redirectAfterLogin")||"App.public.home");
      });
    } else {
      sAlert.error(TAPi18n.__('admin.alerts.credentials'));
    }
  },
  'click [data-event-action=back]': function(event, template){
    event.preventDefault();
    event.stopPropagation();
    FlowRouter.go("App.public.home");
  }
});

