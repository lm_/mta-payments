import { Meteor } from 'meteor/meteor';
import '/imports/startup/server';
import { Settings } from '../imports/api/settings/settings.js';

Meteor.startup(() => {
  // code to run on server at startup
  
  SyncedCron.add({
    name: 'Complete pending transactions',
    schedule: function(parser) {
      return parser.text('every 10 mins');
    },
    job: function() {
      let settings = Settings.findOne();
      if (settings.autocomple) {
        Meteor.call("transactions/autocomplete");
      }
    }
  });
  SyncedCron.start();
});

